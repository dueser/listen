package eu.dueser.grafen;

import nrw.List;

import java.util.Objects;

public class Vertex {

    public Vertex() {
    }

    public Vertex(String name) {
        this.name = name;
    }

    private boolean besucht;

    private String name;
    private MyList<Edge> edges = new MyList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(MyList<Edge> edges) {
        this.edges = edges;
    }

    public boolean isBesucht() {
        return besucht;
    }

    public void setBesucht(boolean besucht) {
        this.besucht = besucht;
    }

    public List<Vertex> neighbours() {
        List<Vertex> result = new List<>();

        edges.toFirst();
        while (edges.hasAccess()) {
            result.append(edges.getContent().getVertex());
            edges.next();
        }
        return result;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return Objects.equals(name, vertex.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "name='" + name + '\'' +
                ", edges=" + edges +
                '}';
    }
}
