package eu.dueser.grafen;

public class Edge {
    private Vertex vertex;
    private double weight;

    public Edge(Vertex vertex, double weight) {
        this.vertex = vertex;
        this.weight = weight;
    }

    public Vertex getVertex() {
        return vertex;
    }

    public void setVertex(Vertex vertex) {
        this.vertex = vertex;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Edge{" +
                "vertex=" + vertex.getName() +
                ", weight=" + weight +
                '}';
    }
}
