package eu.dueser.grafen;

import nrw.List;

public class Graph {

    private MyList<Vertex> vertices = new MyList<>();

    public MyList<Vertex> getVertices() {
        return vertices;
    }

    public void setVertices(MyList<Vertex> vertices) {
        this.vertices = vertices;
    }


    public void addVertex(Vertex vertex) {
        vertices.append(vertex);
    }

    public void addVertices(Vertex... vertices) {
        for (Vertex v : vertices) {
            addVertex(v);
        }
    }

    public void addEdge(Vertex v1, Vertex v2, double weight) {
        if (!vertices.find(v2)) {
            throw new IllegalArgumentException("v2 nicht in Graph vorhanden");
        }
        if (!vertices.find(v1)) {
            throw new IllegalArgumentException("v1 nicht in Graph vorhanden");
        }
        List<Edge> edges = vertices.getContent().getEdges();
        edges.toFirst();
        while (edges.hasAccess()) {
            if (edges.getContent().getVertex().equals(v2)) {
                edges.getContent().setWeight(weight);
            }
            edges.next();
        }
        edges.append(new Edge(v2, weight));
    }

    public void addEdgeUngerichtet(Vertex v1, Vertex v2, double weight) {
        addEdge(v1, v2, weight);
        addEdge(v2, v1, weight);
    }

    public boolean kennenSich(String name1, String name2) {
        vertices.toFirst();
        while (vertices.hasAccess()) {
            if (vertices.getContent().getName().equals(name1)) {
                List<Edge> edges = vertices.getContent().getEdges();
                edges.toFirst();
                while (edges.hasAccess()) {
                    if (edges.getContent().getVertex().getName().equals(name2)) {
                        return true;
                    }
                    edges.next();
                }
            }
            vertices.next();
        }
        return false;
    }

    double distanzEinesWeges(Vertex start, Vertex ziel) {
        if (start.equals(ziel)) {
            return 0.0;
        }
        start.setBesucht(true);
        List<Edge> edges = start.getEdges();
        edges.toFirst();
        double minEntfernung = Double.MAX_VALUE;
        while (edges.hasAccess()) {
            Edge nachbarEdge = edges.getContent();
            Vertex nachbarVertex = nachbarEdge.getVertex();
            if (!nachbarVertex.isBesucht()) {
                double nachbarEntf = nachbarEdge.getWeight() + distanzEinesWeges(nachbarVertex, ziel);
                if (nachbarEntf <= minEntfernung) {
                    minEntfernung = nachbarEntf;
                }
            }
            edges.next();
        }
        return minEntfernung;
    }

    public void alleNamenDrucken(Vertex start) {
        System.out.println(start.getName());
        start.setBesucht(true);
        List<Vertex> nachbarliste = start.neighbours();
        nachbarliste.toFirst();
        while (nachbarliste.hasAccess()){
            if (!nachbarliste.getContent().isBesucht()) {
                alleNamenDrucken(nachbarliste.getContent());
            }
            nachbarliste.next();
        }
    }

    public void allenamendruckenbreiten(Vertex start) {
        MyList<Vertex> warteQueue = new MyList<>();
        warteQueue.append(start);
        while (! warteQueue.isEmpty()){
            warteQueue.toFirst();
            Vertex top = warteQueue.getContent();
            System.out.println(top.getName());
            warteQueue.remove();
            List<Vertex> nachbarnliste = top.neighbours();
            nachbarnliste.toFirst();
            while (nachbarnliste.hasAccess()) {
                if (!nachbarnliste.getContent().isBesucht()){
                    warteQueue.append(nachbarnliste.getContent());
                    nachbarnliste.getContent().setBesucht(true);
                }
                nachbarnliste.next();
            }
        }
    }


    @Override
    public String toString() {
        return vertices.toString();
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        Graph graph = new Graph();

        Vertex a = new Vertex("A");
        Vertex b = new Vertex("B");
        Vertex c = new Vertex("C");
        Vertex d = new Vertex("D");
        Vertex e = new Vertex("E");
        Vertex f = new Vertex("F");
        Vertex g = new Vertex("G");
        Vertex h = new Vertex("H");
        Vertex i = new Vertex("I");
        Vertex j = new Vertex("J");

        graph.addVertices(a, b, c, d, e, f, g, h, i, j);

        graph.addEdgeUngerichtet(a, b, 16);
        graph.addEdgeUngerichtet(a, c, 17);
        graph.addEdgeUngerichtet(b, e, 26);
        graph.addEdgeUngerichtet(c, d, 15);
        graph.addEdgeUngerichtet(c, f, 8);
        graph.addEdgeUngerichtet(d, f, 34);
        graph.addEdgeUngerichtet(d, h, 32);
        graph.addEdgeUngerichtet(e, h, 12);
        graph.addEdgeUngerichtet(f, g, 9);
        graph.addEdgeUngerichtet(f, i, 14);
        graph.addEdgeUngerichtet(g, h, 7);
        graph.addEdgeUngerichtet(g, i, 4);
        graph.addEdgeUngerichtet(g, j, 16);
        graph.addEdgeUngerichtet(h, j, 10);
        graph.addEdgeUngerichtet(i, j, 9);

        graph.allenamendruckenbreiten(a);
    }
}
