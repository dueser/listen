package eu.dueser.grafen;

import nrw.List;

public class MyList<T> extends List<T> {

    public int size() {
        toFirst();
        int counter = 0;
        while (hasAccess()) {
            counter++;
            next();
        }
        return counter;
    }

    public boolean find(T t) {
        toFirst();
        while (hasAccess()) {
            if (getContent().equals(t)) {
               return true;
            }
            next();
        }
        return false;
    }

    @Override
    public String toString() {
        toFirst();
        String output = "MyList[";
        while (hasAccess()) {
            output += getContent();
            next();
            if (hasAccess()) output += ", ";
        }
        output += "]";
        return output;
    }
}
